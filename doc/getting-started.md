# Getting Started with Your CMake Project

Follow these steps to build and run your CMake project:

## 1. Clone the Repository

Clone the project repository

## 2. Create a build directory

```bash
mkdir build

cd build
```

## 3. Generate build files with CMake

```bash
cmake ..
```

## 4. Build the project

```bash
make
```

## 5. Run the executable

*The executable name is defined in the CMakeLists.txt*

```bash
./ExecutableName
```


