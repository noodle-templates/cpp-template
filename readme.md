# C++ Project Template using CMake

This project is a simple template for C++ projects using CMake. It provides a basic structure to kickstart your C++ development, with support for building with CMake and editing in VSCode.

## Editor

The recommended editor for this project is Visual Studio Code (VSCode). It's lightweight, extensible, and provides excellent support for C++ development.

## Getting Started

To get started with this project, follow these steps:

1. Fork the repository

2. Clone the repository on your local machine:

   ```bash
   git clone https://gitlab.com/your-username/your-project.git
   ```

3. Navigate to the project directory:

   ```bash
   cd your-project
   ```

4. Open the project in Visual Studio Code:

   ```bash
   code .
   ```

5. Review the documentation in the `doc` folder for detailed instructions on project setup, building, and usage.

## Project Structure

The project follows a classic structure with the following directories:

- **src**: Contains the source code files for your project.
- **include**: Contains header files for your project.
- **doc**: Contains documentation for the project, including setup instructions and usage guidelines.
- **build**: Used for out-of-source builds with CMake. All build artifacts will be generated here.